<%-- 
    Document   : busca_cep
    Created on : 22/05/2016, 22:57:58
    Author     : Wallace Biscaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>Busca CEP</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-lg-2"></div>
                <div class="col-sm-8 col-lg-8">
                    <legend>Busca de CEP</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="cep">CEP</label>
                                <input id="cep" class="form-control" type="text" required="required" autofocus="" name="cep">                           
                            </div>
                        </fieldset>
                        <div class="col-md-6">
                            <br/>
                            <button id="calcular" class="btn btn-primary btn-lg" onclick="buscar_cep();">
                                <span class="glyphicon glyphicon-search"></span> 
                                Buscar
                            </button>
                        </div>
                    </div>
                    
                    <div id="resultado" style="color: red;"></div><br/>
                    
                    <div class="row" id="form">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="rua">Rua</label>
                                <input id="rua" class="form-control" type="text" name="rua" disabled>
                            </div>
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="bairro">Bairro</label>
                                <input id="bairro" class="form-control" type="text" name="bairro" disabled>
                            </div>
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="cidade">Cidade</label>
                                <input id="cidade" class="form-control" type="text" name="cidade" disabled>
                            </div>
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="estado">Estado</label>
                                <input id="estado" class="form-control" type="text" name="estado" disabled>
                            </div>
                        </fieldset>
                    </div>
                    <div class="row" id="loading" style="display:none;"><center><img src="img/loading.gif"></center></div>
                </div>
                <div class="col-sm-2 col-lg-2"></div>
            </div>
        </div>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        $("#cep").mask("99999-999");
    });

    function preenche_form(cep) {  
        //Pesquisa o CEP utilizando webservice ViaCEP
        //Utiliza chamadas ajax recursivas, substituindo os ultimos digitos por zero ate encontrar o endereco
        $.ajax({
            type: "GET",
            url: "//viacep.com.br/ws/"+cep+"/json/?callback=?",
            dataType: 'json', 
            beforeSend: function() {
                $("#form").hide();
                $("#loading").show();
            },
            success: function(endereco) {
                if(!("erro" in endereco))
                {
                    $("#rua").val(endereco.logradouro);
                    $("#bairro").val(endereco.bairro);
                    $("#cidade").val(endereco.localidade);
                    $("#estado").val(endereco.uf);
                    
                    $("#loading").hide();
                    $("#form").show();
                }
                else
                {  
                    cep = cep.split("");
            
                    for(cont = cep.length - 1; cont >= 0; cont--)
                        if(cep[cont] != "0")
                        {
                            cep[cont] = "0";
                            break;
                        }

                    cep = cep.join("");
                    
                    if(cep != "00000000")
                    {
                        $("#cep").val(cep.substr(0, 5) + "-" + cep.substr(5, 3));
                        preenche_form(cep);
                    }
                    else
                    {
                        $("#loading").hide();
                        $("#form").show();
                    
                        $("#resultado").html("<b>CEP inválido!</b>"); 
                    }
                }
            }
        });
    }
    
    function buscar_cep() {
        $("#resultado").html("");       
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#estado").val("");
        
        var cep = $("#cep").val().trim().replace("-", "");

	if(cep != "")
            preenche_form(cep);
        else
            $("#resultado").html("<b>CEP inválido!</b>");      
    }
</script>