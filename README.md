### Solução Proposta

A solução proposta foi criar uma aplicação web leve e funcional, que contenha: 

- Uma pagina inicial para informação do cep e exibição do resultado.
- Funções para preencher o cep através de WebService.

### View

- index.jsp:
Uma página que contem um formulário com um input para receber o cep, outros para exibir os dados encontrados e as funções preenche_form() e buscar_cep().

- função buscar_cep():
Limpa os inputs de exibição, valida o CEP e envia o mesmo para a função preenche_form(), caso esteja dentro do padrão, ou retorna uma mensagem de erro no caso contrário.

- função preenche_form():
Contém um ajax que faz a comunicação com o WebService da ViaCEP. A pesquisa de cep é feita de forma recursiva. Caso não encontre na primeira tentativa, um digito da direita é substituindo por zero e a pesquisa é feita novamente, até encontrar o resultado para exibição ou o cep for igual a "00000000".

### Bibliotecas/API/Tecnologias utilizadas

- Linguagem Java
- Bootstrap
- JSTL
- JQuery
- Webservice ViaCEP

### Testes

Foi feita uma massa de teste, utilizando diferentes ceps válidos e inválidos.

### Observações Finais

- Projeto criado na IDE Netbeans.